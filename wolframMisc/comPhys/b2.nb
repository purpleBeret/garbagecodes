Notebook[{Cell[
CellGroupData[{Cell[
BoxData[RowBox[{"Manipulate","[",RowBox[{"n",","," ",RowBox[
{"{",RowBox[{"n",","," ","1",","," ","10"}],"}"}]}],"]"}]],"Input",CellLabel -> "In[1]:= ",
ExpressionUUID -> "b051b6dd-b287-494b-a49d-40abb8d8e2ea"],Cell[
BoxData[TagBox[StyleBox[
DynamicModuleBox[{Set[n$$,3.2`],Set[Typeset`show$$,True],Set[Typeset`bookmarkList$$,
{}],Set[Typeset`bookmarkMode$$,"Menu"],Typeset`animator$$,Set[Typeset`animvar$$,1],Set[
Typeset`name$$,"\"untitled\""],Set[Typeset`specs$$,{{Hold[n$$],1,10}}],Set[Typeset`size$$,
Automatic],Set[Typeset`update$$,0],Set[Typeset`initDone$$,False],Set[Typeset`skipInitDone$$,
True]},DynamicBox[Manipulate`ManipulateBoxes[1,StandardForm,RuleDelayed["Variables",
{Set[n$$,1]}],RuleDelayed["ControllerVariables",{}],RuleDelayed["OtherVariables",
{Typeset`show$$,Typeset`bookmarkList$$,Typeset`bookmarkMode$$,Typeset`animator$$,Typeset`animvar$$,Typeset`name$$,Typeset`specs$$,Typeset`size$$,Typeset`update$$,Typeset`initDone$$,Typeset`skipInitDone$$}],
RuleDelayed["Body",n$$],RuleDelayed["Specifications",{{n$$,1,10}}],RuleDelayed["Options",
{}],RuleDelayed["DefaultOptions",{}]],SingleEvaluation -> True],RuleDelayed[DynamicModuleValues,
{}],RuleDelayed[Deinitialization,None],RuleDelayed[UntrackedVariables,{Typeset`size$$}],
SynchronousInitialization -> True,RuleDelayed[UnsavedVariables,{Typeset`initDone$$}],
RuleDelayed[UndoTrackedVariables,{Typeset`show$$,Typeset`bookmarkMode$$}],ExpressionUUID -> "510785f8-dcb2-4a16-b679-3ff81abeda46"],
"Manipulate",Deployed -> True,StripOnInput -> False],Manipulate`InterpretManipulate[
1]],StandardForm],"Output",CellLabel -> "Out[1]= ",ExpressionUUID -> "ae33e8be-d50e-49a8-9765-464167b67306"]},
Open],ExpressionUUID -> "ae56b76b-43ff-49dd-a690-27f8d2202cdf"],Cell[
CellGroupData[
{Cell[
BoxData[RowBox[{"Manipulate","[",RowBox[{RowBox[{"n","^","2"}],","," ",RowBox[
{"{",RowBox[{"n",","," ","1",","," ","10"}],"}"}]}],"]"}]],"Input",CellLabel -> "In[2]:= ",
ExpressionUUID -> "8afe7158-d2cf-4441-9818-1ae0e9ea3c07"],Cell[
BoxData[TagBox[StyleBox[
DynamicModuleBox[{Set[n$$,4.16`],Set[Typeset`show$$,True],Set[Typeset`bookmarkList$$,
{}],Set[Typeset`bookmarkMode$$,"Menu"],Typeset`animator$$,Set[Typeset`animvar$$,1],Set[
Typeset`name$$,"\"untitled\""],Set[Typeset`specs$$,{{Hold[n$$],1,10}}],Set[Typeset`size$$,
Automatic],Set[Typeset`update$$,0],Set[Typeset`initDone$$,False],Set[Typeset`skipInitDone$$,
True]},DynamicBox[Manipulate`ManipulateBoxes[1,StandardForm,RuleDelayed["Variables",
{Set[n$$,1]}],RuleDelayed["ControllerVariables",{}],RuleDelayed["OtherVariables",
{Typeset`show$$,Typeset`bookmarkList$$,Typeset`bookmarkMode$$,Typeset`animator$$,Typeset`animvar$$,Typeset`name$$,Typeset`specs$$,Typeset`size$$,Typeset`update$$,Typeset`initDone$$,Typeset`skipInitDone$$}],
RuleDelayed["Body",Power[n$$,2]],RuleDelayed["Specifications",{{n$$,1,10}}],RuleDelayed[
"Options",{}],RuleDelayed["DefaultOptions",{}]],SingleEvaluation -> True],RuleDelayed[
DynamicModuleValues,{}],RuleDelayed[Deinitialization,None],RuleDelayed[UntrackedVariables,
{Typeset`size$$}],SynchronousInitialization -> True,RuleDelayed[UnsavedVariables,
{Typeset`initDone$$}],RuleDelayed[UndoTrackedVariables,{Typeset`show$$,Typeset`bookmarkMode$$}],
ExpressionUUID -> "5d42b906-bcdd-4cad-ada2-644bc820d78a"],"Manipulate",Deployed -> True,
StripOnInput -> False],Manipulate`InterpretManipulate[1]],StandardForm],"Output",
CellLabel -> "Out[2]= ",ExpressionUUID -> "c371d445-4100-4af1-9d45-b43a047d0a54"]},
Open],ExpressionUUID -> "d3d2185d-e8a8-4aae-9d69-e7a5112c2d7d"],Cell[
CellGroupData[
{Cell[
BoxData[RowBox[{"Manipulate","[",RowBox[{RowBox[{"Plot","[",RowBox[{RowBox[
{"Sin","[",RowBox[{"n","*","x"}],"]"}],","," ",RowBox[{"{",RowBox[{"x",",",RowBox[
{"-","10"}],","," ","10"}],"}"}]}],"]"}],","," ",RowBox[{"{",RowBox[{"n",","," ","1",","," ","10"}],"}"}]}],"]"}]],
"Input",CellLabel -> "In[3]:= ",ExpressionUUID -> "aa46635d-8110-4572-bbc4-0493bfa15e48"],Cell[
BoxData[
TagBox[StyleBox[DynamicModuleBox[{Set[n$$,1`],Set[Typeset`show$$,True],Set[Typeset`bookmarkList$$,
{}],Set[Typeset`bookmarkMode$$,"Menu"],Typeset`animator$$,Set[Typeset`animvar$$,1],Set[
Typeset`name$$,"\"untitled\""],Set[Typeset`specs$$,{{Hold[n$$],1,10}}],Set[Typeset`size$$,
Automatic],Set[Typeset`update$$,0],Set[Typeset`initDone$$,False],Set[Typeset`skipInitDone$$,
True]},DynamicBox[Manipulate`ManipulateBoxes[1,StandardForm,RuleDelayed["Variables",
{Set[n$$,1]}],RuleDelayed["ControllerVariables",{}],RuleDelayed["OtherVariables",
{Typeset`show$$,Typeset`bookmarkList$$,Typeset`bookmarkMode$$,Typeset`animator$$,Typeset`animvar$$,Typeset`name$$,Typeset`specs$$,Typeset`size$$,Typeset`update$$,Typeset`initDone$$,Typeset`skipInitDone$$}],
RuleDelayed["Body",Plot[Sin[Times[n$$,x]],{x,-10,10}]],RuleDelayed["Specifications",
{{n$$,1,10}}],RuleDelayed["Options",{}],RuleDelayed["DefaultOptions",{}]],SingleEvaluation -> True],
RuleDelayed[DynamicModuleValues,{}],RuleDelayed[Deinitialization,None],RuleDelayed[
UntrackedVariables,{Typeset`size$$}],SynchronousInitialization -> True,RuleDelayed[
UnsavedVariables,{Typeset`initDone$$}],RuleDelayed[UndoTrackedVariables,{Typeset`show$$,Typeset`bookmarkMode$$}],
ExpressionUUID -> "fe2d71f9-558c-42e4-a82d-503c5cbc1c1a"],"Manipulate",Deployed -> True,
StripOnInput -> False],Manipulate`InterpretManipulate[1]],StandardForm],"Output",
CellLabel -> "Out[3]= ",ExpressionUUID -> "63424456-4c29-4bf8-991f-4744cf64afa6"]},
Open],ExpressionUUID -> "0aefdb5b-aee7-4dc4-a861-afc630afb856"],Cell[
CellGroupData[
{Cell[
BoxData[RowBox[{"Manipulate","[",RowBox[{RowBox[{"Plot","[",RowBox[{RowBox[
{"Sin","[",RowBox[{"n","*","x"}],"]"}],","," ",RowBox[{"{",RowBox[{"x",",",RowBox[
{"-","10"}],","," ","10"}],"}"}]}],"]"}],","," ",RowBox[{"{",RowBox[{RowBox[{"{",RowBox[
{"n",","," ","3"}],"}"}],","," ","1",","," ","10"}],"}"}]}],"]"}]],"Input",CellLabel -> "In[4]:= ",
ExpressionUUID -> "fd348247-17a4-4191-9df5-d4959ca11be5"],Cell[
BoxData[TagBox[StyleBox[
DynamicModuleBox[{Set[n$$,3],Set[Typeset`show$$,True],Set[Typeset`bookmarkList$$,
{}],Set[Typeset`bookmarkMode$$,"Menu"],Typeset`animator$$,Set[Typeset`animvar$$,1],Set[
Typeset`name$$,"\"untitled\""],Set[Typeset`specs$$,{{{Hold[n$$],3},1,10}}],Set[Typeset`size$$,
Automatic],Set[Typeset`update$$,0],Set[Typeset`initDone$$,False],Set[Typeset`skipInitDone$$,
True]},DynamicBox[Manipulate`ManipulateBoxes[1,StandardForm,RuleDelayed["Variables",
{Set[n$$,3]}],RuleDelayed["ControllerVariables",{}],RuleDelayed["OtherVariables",
{Typeset`show$$,Typeset`bookmarkList$$,Typeset`bookmarkMode$$,Typeset`animator$$,Typeset`animvar$$,Typeset`name$$,Typeset`specs$$,Typeset`size$$,Typeset`update$$,Typeset`initDone$$,Typeset`skipInitDone$$}],
RuleDelayed["Body",Plot[Sin[Times[n$$,x]],{x,-10,10}]],RuleDelayed["Specifications",
{{{n$$,3},1,10}}],RuleDelayed["Options",{}],RuleDelayed["DefaultOptions",{}]],SingleEvaluation -> True],
RuleDelayed[DynamicModuleValues,{}],RuleDelayed[Deinitialization,None],RuleDelayed[
UntrackedVariables,{Typeset`size$$}],SynchronousInitialization -> True,RuleDelayed[
UnsavedVariables,{Typeset`initDone$$}],RuleDelayed[UndoTrackedVariables,{Typeset`show$$,Typeset`bookmarkMode$$}],
ExpressionUUID -> "24ab9090-79ba-46d4-8b3c-e2179bd4a36e"],"Manipulate",Deployed -> True,
StripOnInput -> False],Manipulate`InterpretManipulate[1]],StandardForm],"Output",
CellLabel -> "Out[4]= ",ExpressionUUID -> "6df4db24-f178-4ca2-a3ff-508237fa6ffd"]},
Open],ExpressionUUID -> "1e93fe55-889f-4943-a85e-b1d81a9ba01a"]},StyleDefinitions -> "Default.nb",
FrontEndVersion -> "12.2 for Wolfram Cloud 1.57.0.2 (December 8, 2020)"]