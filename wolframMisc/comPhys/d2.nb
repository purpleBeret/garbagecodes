Notebook[{Cell[
CellGroupData[{Cell[
TextData[{"Understanding"," ","Quadratic"," ","Functions"}],
"Subtitle",ExpressionUUID -> "9919b1ed-d592-4ffc-9ab6-e4405bfcadbd"],Cell[
CellGroupData[
{Cell[
BoxData[RowBox[{"Manipulate","[",RowBox[{RowBox[{"Plot","[",RowBox[{RowBox[
{RowBox[{"a"," ","*"," ",RowBox[{"x","^","2"}]}]," ","+"," ",RowBox[{"b"," ","x"}],"+"," ","c"}],","," ",RowBox[
{"{",RowBox[{"x",","," ",RowBox[{"-","5"}],","," ","5"}],"}"}],","," ",RowBox[{"PlotRange"," ","\[Rule]"," ",RowBox[
{"{",RowBox[{RowBox[{"{",RowBox[{RowBox[{"-","5"}],",","5"}],"}"}],",","\n",RowBox[
{"{",RowBox[{RowBox[{"-","10"}],","," ","10"}],"}"}]}],"}"}]}]," ",","," ",RowBox[
{"Frame"," ","\[Rule]"," ","True"}]}],"]"}],","," ",RowBox[{"{",RowBox[{RowBox[{"{",RowBox[
{"a",","," ","1"}],"}"}],",",RowBox[{"-","5"}],",","5"}],"}"}],","," ",RowBox[{"{",RowBox[
{RowBox[{"{",RowBox[{"b",","," ","0"}],"}"}],","," ",RowBox[{"-","5"}],",","5"}],"}"}],","," ",RowBox[
{"{",RowBox[{RowBox[{"{",RowBox[{"c",",","0"}],"}"}],","," ",RowBox[{"-","5"}],",","5"}],"}"}]}],"]"}]],
"Input",CellLabel -> "In[2]:= ",ExpressionUUID -> "2842a015-4cc3-4458-aaef-d945f1d4a31a"],Cell[
BoxData[
TagBox[StyleBox[DynamicModuleBox[{Set[a$$,-2.27`],Set[b$$,3.3599999999999994`],Set[
c$$,0],Set[Typeset`show$$,True],Set[Typeset`bookmarkList$$,{}],Set[Typeset`bookmarkMode$$,
"Menu"],Typeset`animator$$,Set[Typeset`animvar$$,1],Set[Typeset`name$$,"\"untitled\""],Set[
Typeset`specs$$,{{{Hold[a$$],1},-5,5},{{Hold[b$$],0},-5,5},{{Hold[c$$],0},-5,5}}],Set[
Typeset`size$$,Automatic],Set[Typeset`update$$,0],Set[Typeset`initDone$$,False],Set[
Typeset`skipInitDone$$,True]},DynamicBox[Manipulate`ManipulateBoxes[1,StandardForm,
RuleDelayed["Variables",{Set[a$$,1],Set[b$$,0],Set[c$$,0]}],RuleDelayed["ControllerVariables",
{}],RuleDelayed["OtherVariables",{Typeset`show$$,Typeset`bookmarkList$$,Typeset`bookmarkMode$$,Typeset`animator$$,Typeset`animvar$$,Typeset`name$$,Typeset`specs$$,Typeset`size$$,Typeset`update$$,Typeset`initDone$$,Typeset`skipInitDone$$}],
RuleDelayed["Body",Plot[Plus[Times[a$$,Power[x,2]],Times[b$$,x],c$$],{x,-5,5},PlotRange -> {{-5,5},{-10,10}},
Frame -> True]],RuleDelayed["Specifications",{{{a$$,1},-5,5},{{b$$,0},-5,5},{{c$$,0},-5,5}}],
RuleDelayed["Options",{}],RuleDelayed["DefaultOptions",{}]],SingleEvaluation -> True],
RuleDelayed[DynamicModuleValues,{}],RuleDelayed[Deinitialization,None],RuleDelayed[
UntrackedVariables,{Typeset`size$$}],SynchronousInitialization -> True,RuleDelayed[
UnsavedVariables,{Typeset`initDone$$}],RuleDelayed[UndoTrackedVariables,{Typeset`show$$,Typeset`bookmarkMode$$}],
ExpressionUUID -> "a1818caf-3932-4fe6-a122-411df52be3df"],"Manipulate",Deployed -> True,
StripOnInput -> False],Manipulate`InterpretManipulate[1]],StandardForm],"Output",
CellLabel -> "Out[2]= ",ExpressionUUID -> "dc9943e4-6882-4b6d-9dc9-5784e1a46257"]},
Open],ExpressionUUID -> "c9327014-9051-4944-9d26-9970452bfcf7"],Cell[
CellGroupData[
{Cell[
BoxData[RowBox[{"Plot","[",RowBox[{RowBox[{"x","^","2"}],","," ",RowBox[{"{",RowBox[
{"x",","," ",RowBox[{"-","5"}],","," ","5"}],"}"}]}],"]"}]],"Input",CellLabel -> "In[1]:= ",
ExpressionUUID -> "3e3185f1-463a-4cbe-b192-033d9bb50847"],Cell[
BoxData[GraphicsBox[
{{{{},{},TagBox[{Directive[Opacity[1.0],RGBColor[0.368417,0.506779,0.709798],AbsoluteThickness[
1.6]],LineBox[CompressedData["1:eJw1mHc0Fn7YxpHsrEeFoiRShMwnqfsrScqIpEHDjAaFkFXZIsmeyYiQkVX85PvgQSh7hjzZez+2vL3nvO9f97n/uc513X/c53MuIRMbPXM6Ghoabloamv+dbqNaC9vbBBK5oqJve5uIRlz2UHs2CSQ799ceoqtEpM3Zv/JljUBKZlk10Z4jooMnH2/ZLBJIkWGfOhIpRFTxKoSJMkIgZVuH+l8rJyImyU5B0k8CqejUdYG/HkQUZn/30otYAkngZNmmIiMRZdLapdAoEkjK5RsxtwmKaKe5ugzfLW7SdoO0RQRBHhG6qh4mveUiJdT5fUu9KIv0KmNMj9ZxkrRi8/drvTuBXGZPPwwe5CDpuKsKKNVIoa+OMfdzdNhJ3iHlV6WKJJB2yNWDf6PZSJjwuto4+ChSj/+mfWGKheQ8e1C8OlUUpdY867AzZCaV2oQbTuw9jG57he1u/MhIosqlyBx2F0Lud3dksxIYSMy8OnUO9oKI5vgBrOpNTzKSVrj6XJcf/QqI9s1spyNVFZ+9Myq+B+1nsUxfOktLKhrm4LubwI0eyB/b2bN/G3NQRO7QIXZkqvj7Y2XlBta4d7qOcS8TOuaktG2dsorlGjhtF9foUGLfFfIzFSreRWcQNqm8CTJDMuttclRs1CnjdWvfJpAnuKSlxai4zSPXZnBtA0ZWGmNH2KlYwSjopPiXDTjGrWl3pXcJN/O2RWXIbEDe+XOHjjsuYZNiX2/L4+tAzpF7Qfm0iDci3+z9c2IVRl/uVlbnXcAzdUKCVQ5LwOKWMxvKsoAXzgd1t15bAkknjWTK5jxWDDRnXScugYO1G4vzn3nsZqvfErixCAyGw12fMubxepbsdP3LRTgsV/iU+/Q8LqCsGOSGLIDJiH5On/EcJspHrnNVz4HPnxmTY/pzeE+3kSdHxhxk9PrtcTw/h2XOv/Y4HDQHCy2lbpzic1jMVXIozGAO6J5rRPmMzWK67CFLweZZgBPjzbq7ZvFc8DZXm9AMlISLqY0ZTOMxOVP6JyMTwGQ4akqrPI1ZmC6yyBZOgMHBVA9+oWmc5Bt86ZDXBCxkCJM0J6fwiUDlF36HJuAoSfDU5+dTuCGDM8/OeByiJnhOOH+cxKxn0hWrRkfhKdAKsK2PY4/S2Dr9PcNQQU86JdI/jrUEnZTLJoaAs8795hnyOH4jUTCuj4fgk/5m5OOgcWyvfWmZ2WoIhqxWuNsPjeOyNGpnSNkg6IVNM727NIZ1cpW+HbAdAOnx7iWp+BE8JsN2QHO5H+znjh/d6zWCd+n5Snnk9MPXlZe3/t4fwVpnLmwwWPWDCsOx6h/EEXzYPZa82fsb9ISdI63ahvH+WF/B6O998PQWv1Iy6zBGFx1mw7J7oKT55vM9zoP4glvWzujCTvjblZ2/dXcQD8KAe6pTJ5yl0I0NqQ/i6EajfDrlTqifybicv3sQGxkIa6dVdkAv64bQ5c8DWIJ2+WJiRztsq8WS/cf+4O36z8bXWdvgXEkP85YBBZsreO5ozWmC7IwPJ2tOUfCsiFrdI6cm4I21sXp7kIJvumcLmKs0QccKQ8iltn4c3zrT1vKxEURcDDjj139jDwOy4tutn1DxksqG1PuwS65CaUVtHUSY+D9s4+zDCZlpv/0t6sBKVeCH5a9e7OF0j5+Fvg44d6oFvH3Ui4N8W/vkVWrhjl8Y82BoD853vhWmRK6BzSBZBh9KNz5eSCStTpGhyabGnD+jGxv/iHDUDSdD8mXDqmy7bqxfqHrB9AwZLnJ7enUydOMXm/7yeSGVEB3eQnf0eBceufMdtlQrQCHuyXb9sw5MovKKBodgeJyes8pFaMXiO8ysjL8VgTS7tedZSgveOiox23uuCOZtJdjtslpwQv8J8difhWCrnC7cpt6CtVaoVf0DBfC0MUk7wrUZX+u66+O/Px8WJ8Q15H80YtkKD/OyqBywL/h4O8KiEX8YJxPZmrJh2U3EfoWmERdKejj1M2fDKueBhGL5BvyRy/r0Hu9PsKnATT2d8AOPKLH2kaPTgcFz9f1521r8nd/QTlQ5BXw1nxalsdXinft12Otjk4Fpz0I9U9p33K9Utpu6lQQs6ZPLdT012Kp51/3u2kTgaPytqaNWjbMqhy5Xb7+D4ChDk9z+KryQ4K5tMRoP3CZdjlzOVdj+C7sIV1sc8FCbk1qzyfhAFNct39IY4N1XtXqNtxKLHkxapdZGQPSQCntxXgVW7Jj+NtUdDvzZZcL8WhXYo0bKaGYmDARUvmr3vizH1YYRwtxioaDaYKBR3oHxanjvf1+y3sB548v2nqwY5xd2tusJBsGFJY0ENVSGKZdw4o7oQNDkP02tTS/FIT1XdFI/+YOBhXBii2sxNvILoZ3T9IJXT+z8Qgq/4rTKRU4THk/45lppozfzBT/v+++B9/BLOBxqeqblThHGN2gZSRrP4fq7fJGQqELcQF5/ISnqBgHpO3bpNRfgtanyt9u7XWABJ/c0n83HmfbFjrVSjiBav1jx1iUPy7aNa1bceAo3OlQzdAs+48650a8CkXZAmhp0ahbJxS7GWlc3Lz2Gsdl7pEPfsvHKfSvWbZ9HoMPA9CZJMgvLvG6Nrou7D4KyFySSuDLwHeQMn5XNwVtjbF3I8yO+3Fxk8KPaBKbu+NUmLqVit96g2E+Od+GKg1iUkMUH/KOSfn/KOyMoCfxukdiZjHkrDxk9i70OQsmW8kIaSbivyHGla1gf/IqZ6BNL3mP+hB0zFW668DtB94PK9Xc4cSXwuXW1JmT9eHtW5VQspo150f1AQh1c1pr7kWAU1hMZGHc7fhY0RLndEG04PiJraqwRegr2XtHjR0Nv8ZvpPkMSrRzUbx+2eR70GhctWvqUa4qD5Y0P6e76vpgn96fkVTohoM8XGXLjf4mfHGwzdrzADYhP8/Q66Snelzhn/jF068xcVLTZfLQJLnRW1BB7337GlS6s6jxRA/boML7WpSSfkYwxreFRsIFVy+cfhZ/MnPnN93mGlcMVDEUnlgxjmOCKbFepj4oX1L/QkO1L4YOi7msevi6vYD2mmiN6QgR2kSaFyTeDIZOtSSrgizRAaKYP2TEUvsVHnVvMUYQnFg/GyWERkE9zutrrN0DSSXHNqs/RQI65/Ssh8Ry0sU1mVzXEwaF3bEzcsxrAQMngqp5MANtXYlNRnToQd0NeydQrEX5J3VCNOX4F5FXDHwxyJ8MSe/uDhVIDaJCgxpkmpoDPgO9sW/RNoKEp3DYtS4MAWQ2v/e+MIXqc58SQZjqwsFG1+2jMQKbV3sSsJwOqbiurm76yALNUuSqz1ax/fyaeVMb1ELbehC0P+eSAsdwXlTAOG6hXXg/Ne5ULjEQFp738T8BM9GaVDF8ecGTqT3Y72YP0PpbftpQ8gBe8jDpqDrDFUbKcl5YPb7gzikVFnCBijU9MVqEQ/K0VBrcEXaHmR/cr2StfYYF2PfmDmwdYOv2qMRQvhuYC6pH1ek9gOtxD77WjBAoyqyaKRLzhokuve1v+fxBsPiBhteELP4722z7lwRDIzJGcKxUED9v7c+KnMET1fhJQ734DPujxsokJCdqfvZk8VBMMR6YeywYolcPCMuw48zoELFVts3omKkDbPyZz+FYEEKMX6mcNKmHSQ2lI924kMM3aTuyorITAd+eUbS2jID3G7ohELBm6TkVebPSPgYl5+0TXS9Xwll2Bss6ZAA/fO4YLfKqFCyxZKwFRKaC8vJJ/grcOJPdqc5iqfwA2TacWNa862COfosCz9gGyVpw4rI3qYTLZpoDePA1mtJ39y9h+QqqW50PBOxnw1b2sLEehCXLHtO/xW+fC+/S/P4sfNoHUSs1jg95c8Gs701eZ9G8P2uexfekzXD+GNzrZmyH3ql5plFQerLVjIs1oM3y7+DdolLEAlI6X512ObIXVHzpZy5tfQL6tI6KsthWCdDPseJ59BWmXKWeJzVZ4bv5a4PHyVxCt3avKZNwGjqem9/1ZKgaCuXUrPtYOJQmEynSaUpiK27ckWdYB5SlufRNHSTCqeqIrbq4Dpo1ne8eekGBg4nwpi3An1FiINHiXkKCbaOs14tsJPuq/veO0yqG67TvPO90uEDf3vHjapQIS2Bzkdw13wx6uWDHNJTLE5Afwuez9BcIauudOnq+CiJuJW2Mav6D/e71uVVQVBH78UUXO/gW3E3adLYFqcDonbODq1APeH8mOe8NrQM+1yWGStQ+eidMQE27VAZ3D7l4zPgosPevYuSDTBDfvZrR8laPAJbaddw7cb4K8i1DLdpkCXOfNuTMSm8DkgFVRgQ8FZnNKfDu5mqHie2kw/RIFJo2KP4UsN4PnPjO1lIY/MLWPgeLf3Ar05XnZg56DwCDZN+nysxOMMtU/KCYMwt1sFKDP2QUF4b2xASWDIN7R7vXqSheY3Wfwl50fBK794T7sPV1QRbhp6nl7CIRe3iM7zHSDjwUtr/DJYTiXa3j0u2gvMLJdfmkyMwK6zUjzizUFrh+5SVJgHoU1p0xDlXAKZJw122Y5PArbfgP3Vv+jgPYzJ7f8G6MgPFDoMM38ByJG3j+jrxqFKqf5R4tpf0C0fP5JauwYOCuWVlhPDYCaQ6jJxIUJSFxP2CkcOQyelA5V2+RpGI18P/fSbhJ+F1965FQ6DV05A9pziZOgFEqKcG+f/scVlk3eTZMwp5Yx/opxBtI80WlRySm4nen2JvnBDLArHShTnpwCJYfDPW2ys1DLEG1EejgDaztPFu8zmYNNDmB4+4/nc2N9LrsvzEO0GGNcmtAS1BN8RjloFqDEZeu7MFqCkQBv98RdCzDP57RWdHsJ9rt6fSKLLcA7xXWzQ3FL4GfkwcR6ZwGoKycsdvFS4a6AOymyfgFiAm245HiWgTPhqfTnlEUg7RRmJQusgk2SGeeQAfVf/gcBM0834fb9GAHfl6tg1UCzakCmQ/66Didl4zagYqJ4KLiFEdHzhI88X/8L8aG1pOtbu1BnvfecFwctqv6VE8h8ghtddTFRiKmkQ+9e84Et7R70kHXyUMNNegRyBbkMw3zoQGZ1BMPETvTnvo9m7g5B5PJV9QpHLCM6fFnKrN1KCD1OfVikIMWMViQPiLK2CCM2OkUTyWEWJKFXgttiRJHaI/3wY6FsKNb4u76U11HEWyxgo3aYHSVYiGmJm0mgEFu/t9KdHCg7e1gyPFsK6fnoHI8v4kSvRx9F3X56Ah0z9Lqa4saFPl1mbusUlEUdI8xJoVe50VMxj4GrdPKowWRYhfsAAfWu6ucz/lJAf+jTVsMPEVDGkQfz1gMKaCnVModPlIDU1ypKOicUEN/k5H6h4wQ06qU4m7WhgMzsFlakThFQ62m5/ocCimjdcztL6xoBaSm49sgZKyLRD3z8/m8IiKxyXm16XBGdVO9pYgsloCt1wZPeC4pIczzONziCgD5T9wsc3FBET44fpEbG/9OTWxe6wUZEpYWiTamZBFSh/z2vT5KI9Kplfcg1BHR2X/6ctD0RmVtSlS/UE1Be095LA65E5MTyZbG+gYDmxMqkw72JKF5bybi1/Z9eeWP8dgQRjXUg5YFBAjoXzrA9UkxE6050i+ajBCRk7JSfUkFEu/aR08cnCKixUXzRtJ6IZO6o752fJ6CQ8Gs8o71EpEbL3GBP/XefqUZqxjARXU+u81pdJaC3I/4mj2eI6L5a4CnXzX/+PL1vKq4Q0f/1Iej/+5D/ATO5lU4="]]},
Function[Annotation[Slot[1],"Charting`Private`Tag$636827#1"]]]}},{}},{DisplayFunction -> Identity,Ticks -> {Automatic,Automatic},AxesOrigin -> {0,0},FrameTicks -> {{Automatic,Automatic},{Automatic,Automatic}},GridLines -> {None,None},DisplayFunction -> Identity,PlotRangePadding -> {{Scaled[
0.02],Scaled[0.02]},{Scaled[0.05],Scaled[0.05]}},PlotRangeClipping -> True,ImagePadding -> All,DisplayFunction -> Identity,AspectRatio -> NCache[
Power[GoldenRatio,-1],0.6180339887498948],Axes -> {True,True},AxesLabel -> {None,None},AxesOrigin -> {0,0},RuleDelayed[
DisplayFunction,Identity],Frame -> {{False,False},{False,False}},FrameLabel -> {{None,None},{None,None}},FrameTicks -> {{Automatic,Automatic},{Automatic,Automatic}},GridLines -> {None,None},GridLinesStyle -> Directive[
GrayLevel[0.5,0.4]],Method -> CompressedData["1:eJzlUstOwzAQbMv7wlscuPENfAAqLYVKFKqm6t1N1u0K1xt5bYnw2/wAtqs+CbkgTkRKtPbuzuxM9mZMA7lXq9V413+eka1sLE4DpyC59EEbpHDK3pPTmTBFYgsFfOQTTWdpJiymW03Xq6ZHI/IpptzVFoxILZKWOxV8Bz4YgWFfZ27j83m3VXLug6HHeuuRY+gTY0RtrKPGYGgccBDXEYqhhOhBSkgtVw4UdD7hZKr8a2W9ojIwGe8GYbjfSh6vw/QJ9S+wwkhtQ7lCDbypu8QqaQAkmVnLGSbTowyW5pTg5kqkMANtK3HD6O9JsK2pVEmuSMLxhfQ2xelqM3rA07hKcyPOAtiYSTkL0Z8EPwD3v6sPEEkqvPhJx+m4Ucw/0F35oEVkMtTCAg+J1GseOyrlnYQpkb0XxYJiPuRhWKbFzSZCyJcULW+6mTcVbbH67Ykii/UNgX1hbEkBLsH/jKFkzS6ieTlCNhLKwX9y4gucBRzd"],PlotRange -> {{-5,5},{0.0,24.999997959183716}},PlotRangeClipping -> True,PlotRangePadding -> {{Scaled[
0.02],Scaled[0.02]},{Scaled[0.02],Scaled[0.02]}},Ticks -> {Automatic,Automatic}},
ExpressionUUID -> "89a1a506-cea7-4648-93dd-a7d42b5f0dfe"],StandardForm],"Output",
CellLabel -> "Out[1]= ",ExpressionUUID -> "c2336680-4a98-4e09-bfb3-d2614b229a4e"]},
Open],ExpressionUUID -> "48472a90-8051-414b-bd02-5525855af894"],Cell[
CellGroupData[
{Cell[
BoxData[RowBox[{"Plot","[",RowBox[{RowBox[{RowBox[{"x","^",RowBox[{"(","2",")"}]}]," ","+"," ","x"}],","," ",RowBox[
{"{",RowBox[{"x",","," ",RowBox[{"-","5"}],","," ","5"}],"}"}]}],"]"}]],"Input",CellLabel -> "In[2]:= ",
ExpressionUUID -> "24ec62d1-6282-40b6-af4a-ff7bae7ed68a"],Cell[
BoxData[GraphicsBox[
{{{{},{},TagBox[{Directive[Opacity[1.0],RGBColor[0.368417,0.506779,0.709798],AbsoluteThickness[
1.6]],LineBox[CompressedData["1:eJwtmHc41v/3xyVkhdwIFdIwPtkSoderzIpIog+fFhIqKyGhkkhD2SkKLSsqQpJz2+O+3W8re9zc9l4h8+t3Xb+/zvX445zrPM+4znWd3TYuZleYmZiY2DcxMf2f9Rs0nllfJ5H7DRV61tdV8MAdoT/tKySyT7fBw8lFFXyKr3sh9y+J/EKf6Qp9WgVLaLiuusySyCZBps1lDBVc/DicnT5AIq9RnIPfVqhgdvlmMXINiazqmiQY+lwFR3pcOnnvNYm8uGNg2FJCBadtuvme6RCJXMfU7fZGTxmzXjFQFjnPTy7880bE1FcRk1rKrieFbSOrpmTo+lLlsFnJK1uZaj7ydGNs8MNmWXxnUvv6CwYv2ZVZldmCJoXzvF45ZZrwkGPmRO/oSO7Dp8LPSqzFcpMPZHod6XWUxAbxv04ZjnGSTfkY8g7N4vhjxe2mm9Yc5A/L4xTRgB34QmCkIJG8hfxTyr+NLrUd+1/anMFFYiNXXzWenXIiYSY5cdB5yELuiP14q5OHD7c9iQ1O+81M/jI4qrYngAPv5HRImTu2iWxalPuIsrwZXzsoy9q+cx2yHu/+IWO7imwPdSWXlCyDQkzc6EmnOWSffMhCP3cZLOLH7X1OziEnkbDNVakbXNdzueDAHHJd1rlAC1uGtuKsJp/JWeQLKaTWi8uwQln92XNrFkUa3PKfWF6CAk/ZW5ZBM6jckttcRHUJhOUHEnu/TyFZ78Przu8XwdeQVjX5ZgSRO0/JMqIX4VyQREiq/Qiy0LE9axmyCC1/3aUj5EdQAM/TNOS8CI33hbdMFA6j1vedlnzqi5AkaK9b3TOEHtXd+/KVsgClRlywU3EQDchWXJ6bmYehRb9smUkG8n3R/tShfx7Io003EwoYiH9+MrejeR60BBRJeo8Z6EiRME9ZwTwosjGmt+9noChLp/yooHmYunFSoe9iL9IN3Eo6JLrBRvtThTroKLHzTOnto3/gSxg7rhhvR8p9ykuNqn8gmbouJfmqHZWObFNUlP4D+JLPGlW/HQ0sEK8HeDY47K8nf2IbkuU3unmmYw7iRCWi//u3FX3T15WU85oDcvXWZ+MtTag0U/UePX0WOhKunWcWqEdnc0g5mgmz4DFZI2hdXYcGCmZGoyNmITk6q2Dz/TrEXv3F0thnFoYm2Jt0J2uRcd8BhXyDWXj5LPcG91sCNQnv64ronYFFKV0briMUNHhfUMtAeAbUDW34JmeLEadf5mQE5wxgk/lT647FSN77+Dv6yjQYJtAxhV6EPJ39OH16pkFdIu9IdC0ZsVn3t6SnTgNW6NeddvuF9qp+v8WvPQ1MVbICXf9+RzYD5pmdl6fg3veIWv6tsSioZ8JG1nwKmPr8dhjejkKpHY+EvPQ32EZ0k/VoGJqpL/Dj+2eDXzeqqxQ/RMx3j78MGpoEJqY6OwWHq4CUhutOb50EOofqaq5LPORHSesNWYzDC7Z/2PIfZAG79aDtJq1xmIoZobqrZYOFxMcA0d3jkFCWnlc0mA0zqXvIRqNjwD6vvr5bPwdkyGKaX++OQUuqq+HYdB68HBFQ8kkeBQe785RPrIUwkNlgEvFsFCR+6vcWJxaC6q1w53T3UZDOUBbz0AIg1nnTu7RGIU41JICqRgY2QS4pnboR+BLrbeB5sQhuoU27uJeGgcoVfbwztASKWcia+7qHoTFWfPV1XQnwVftbHSkdBhZP2ZIPpFJIN1+JcQ0dBrpwadCrqFLoc1zg/y05DC/Zt4yxh5WBWeQ4+5uTQ8D9giL/90oFJPybLpWrOAQFTE6cjvEVMCF2Tb9WcAgiIWRxT2MFPE4ZesBMH4RSKWPuO6gSigoZa/YegyCdxNiWsbUKFIdb5xTiB2DqiEmU1KNq8JiSk9keOAA5u1L0zn2thryF++fXnAZA38br7mhrNRxlky2nqg9AwU635xoyFDDb4xPj2NgPfJatEh5kCkTL0iimP/tB1K5p548+CrQrSTKpJ/XDWJNB0iV2Ktjhagc21w0eDz7ebEyFW+dFD7/j6gfzHl8O2zoq5Ns5Oz+e6YP3O3edLpiiwtq14iS31j4o+KUk6c1bA0E+Tlz4Ux/oK+Vd2n6yBl7G5Hd0HOuD4k4Nk7n8Guh4w7OtVKYPLAz8ei//rgGJjzZ6aXx9kC10cF1psgZSsjkzbncxoHJg0++/EjTIr7O6K+TDgHNGfDNSfjRYa8nIWr3EAOW++dqUcBocozMP9RkwoKnntEboJxpQJlJNswQZ4MFz5aM/QQO++bXAVyu94Kq9K+1xLw3MV81+3Gf0gmr9cY2/czTo4Frebfq1F4TlmGZWtxMgQTKxOPSyF9RmQ3ZGShNwRfTdY7G7vSAm/qj2qToBE9InZ8eMekE/uafxgwUByopvpRpVeuERq/a+cjsCPA/NWv8U7YXwSu51Q3cC1vVel4YM9cDaXQ4TzycE6BpPLroSPWDjoVYmEUPAI3MduXM5PRBKay6VSyKAah1zGcX3QGDouHF8OgF8tqNR+wN74Fi21yWXHALMnVD11ms9UIk8V94BAbFuEWtzp3tASGl4t2YlAZL3NK+WiPcAj94BjrBmAuyDn79OZeuBnKsr1SZdBKSGMoiwcTpIT2Eht74NPVGHWG430sGm6+/I3DABKvFP1C/9pIOdstTx9gkCvN93XzdIooPiWKXi3lkCCtJUEuVD6FDK1Z1IzBPAlBX8W9CVDhJhjnGdfzf05bdzrFrQQd/XQeLUCgEZqR80KjTp0HaB2+zoGgHCr10cwyTokO5y2enOOgEBTzRirVnpUJhx1XFlg5sW2MJPNnYD705xk9INTm15Nd0d3w1vcoN3NGz4+/+QP+1xtRtu3E+kSa8SYPaq+Au7UjfcuX3doWyJgH13LPjil7qg8u3QUPICAYvWIy5KpV1wZk7QtGEjf6qWP1H2rAvEvObidScJSNjFr2Bl2QXS0oH1Kxv6PdY+hE5IdIHblrLxZQYBht0aEwEjnbAokDt5tJOAHeQa4+3ZnQCmVi203xv1S7j8Oc2vE8ryMz4l1RBQfP8PNzboBI4z2ed/lRIQbRNyvZGvE56JXFsS+0mAo84uqkNbB5jyNPhVfNnoL6vek7AbHRAekKM+HktAX3/LyL5DHcDPrOzq9IyAvPLrJ/KZOqA280Ko/D0CLj6K5GBEtANXNMX/ie3G/DlKO3qdb4cgGtfpveYEsJ0oqOSSaoejB1c5WHQ36s/VF6ya3wY3n80qvNxNwEqoClsQvRUoDvJGpEYa1LpUXBFNbQVEWjzDKKTBO1PrsoybrTAz6n5sKZkGJ/gfBDaztYKAtG/Dig8NYqPqmWXkWsCI0lFmLkSD6572Nr/mmyF3UGHzf8s1gC2Xik6Tm8FGWVUqtbsGhoQl7/ucaQYPM9M9Oz7WgFqc2zrldhM8/dU0aiJbA5x+rBcv6TTB1MmsC6EcNdB5PrZwjrsJwtmP5pGGqBAoXuS3K+E3RHK7is8mUaEhiW/FubwRnqKdhSx8VHBNyVzcRmqAE6c/a92sqAZFHucHx+j1YLNF/7nQm2qYdj/Ac/NzPfDPQ5CwRzW4a6XsaTSohyN+lnbqYtVwi0g6Fe1bB4XKgm2DTlUwO/LP8YPUjb05TMlkHagAj+zkC9H2BHD5eJlGfKuAeb99HgtMBByfYYm+618Bi3zib38cpEHo0xJeD4EKWFHj/6P9lgrHHIhfVPVyYHuwmKDvXgXmBbtZtG1LIdjoVs4n7ioIetgzk72vFNiFZijsnyrBNDgg6MlgCXCmjM5Xt1fAF1WHOHPHEuAluoxM9MohVq04wOJKMQjvKFu0FC4BUvMXDRFEBgv7PYn1vj/ghOB0gupsDjx2u/ko/HseCP4112c7kwO/fEtczCZyYesmo+LEr99hb4TtkfqLOfDIb1Zb7drGvYV37XXHsuCTtLTub+IriKkYHkjalgrCz02Fm38lw9TLWLvpWBtQWfUw3f3cBnyZI8v01Y8jU92F1iPax5H8K9sKATUXFCPCPfrQ3AWdUWkpCDoaiM6WH/Gq9QhEbvbXhksjo5GHVeDKEYEYxMT0fd228BPSiPhbodeSjCqorY9VzuQh1q3/oqGkfOTg3VZh/c8PxKzQyt3+/idi39vOErg5H/m3qvQeTi5AJ+50+Ddm/UQrFd7vj2YVIqpMt/stAUB3HyaUHLxRhBx03D+3jxSj8Mh+m5c5Zeh6glfUrvQqtNZrE4HraUhrfiFLSbgasftSvDidCMRt5F2vF1iNUtL5XPU21aLPC968zv9RkLnriTfNr2rRxCmfkELuGnQgzJhfp74O5fkXFmaq1aI+9fBzamaN6LBc0TfTmAbkLSF4ITS6FR1sbIourGpALx2KsjYPtSLFO2M+B1Ya0ItGk4BKjTa0v2q7DvvlRsQiTHqMu9oQ6YpzA8j+RszGKrojsh1oLG7HnHxhEzJyabPZ29CF3nJ7Htza34qiqnvexyr0ImZPwQ47ETpa+cbU3ak8hKwupdbnqdLRvYEDAZWOQ+jbCVTFbUpH+eNu9T0JQ8hG3DEnO4iOrLz0fRJ4h1FxZcELljk6UvcSGTSeHEYPdtjpvaf1oNsyVw865o4ilqJvGYwHDLTlXOL7RKdJtIXb9L7NxABKd64ceJoxix7Qm3Tc342jgwWaoj5PVtCX10Gm/jPTaG+v/jw/52bskmTH12fxB82NDRo7DrDjC06vdgXfX0QvN9OCRFl5cchpTw2VuGVUEpKPz9byYxaBqIG7S2uolofTyuiXEG6mPJwK5N2Ev5FnWKh5ovjsHRu1VyXM2JBywgrZiuPrXKOSNCsWvNQh0KPGIYnF08qj2UZYsTp9jvKv7158J0/nDO/rLVjnCJNGrKkUdv14PUdNgQNHnMtvaNkli7mZD9nI93PiTH7DcxLmcljvhnmUbAQ39izkWfU1V8TCP3a56O3lwe4jXuZv5pVwuPujMMVmXpzS/y5FsV4FmwWZyMXn8GHJqiIG67uDWNY68Ox7v23Y86rTMPY/hJsGOJIizvJj2SQWSn6MBqbZ9B/lFyfhF6dpOtRETdzD8mkxSpKEe4ciXQTSNPHcR4dMkf0kzLInfv+FbE0sMjq6c7ccCX++6642X66J7W7OLChoknB6m2Kn+pgmXnqw/tnYkoSNtq/osx/Swvs/iIiGPCdhnjTqE6JGC2sYtNdyR5Dw7rgK8ePNWthoOC74RfRGfEUWVErXwm5yEn9i4klYAR8Th1ktXPB9f+3HNBJWYq63rxDRxmblKkGlFSTcxI35Je218RWHP1qGFBKmyJ61ynbRxt6cubMUGgmHjqbJGd7WxvGnDl9u+E3CPmfFPD2eauOhJqzVyyDhtWqh0O5v2njJm3n2yiAJX1pXV/Qv0MZbd5SmDI+QcHhT3Hmxcm2sfNFg+/Q0CV+4IWNl26qN9TZx0Dz+kPDyATMZdoY2PveuOnBxkYSvPi94kDGmjZ30nmr6rpBwXKSDo8W8Nv7/fwN+p2XRtr6ujf8H7v6xlw=="]]},
Function[Annotation[Slot[1],"Charting`Private`Tag$636982#1"]]]}},{}},{DisplayFunction -> Identity,Ticks -> {Automatic,Automatic},AxesOrigin -> {0,0},FrameTicks -> {{Automatic,Automatic},{Automatic,Automatic}},GridLines -> {None,None},DisplayFunction -> Identity,PlotRangePadding -> {{Scaled[
0.02],Scaled[0.02]},{Scaled[0.05],Scaled[0.05]}},PlotRangeClipping -> True,ImagePadding -> All,DisplayFunction -> Identity,AspectRatio -> NCache[
Power[GoldenRatio,-1],0.6180339887498948],Axes -> {True,True},AxesLabel -> {None,None},AxesOrigin -> {0,0},RuleDelayed[
DisplayFunction,Identity],Frame -> {{False,False},{False,False}},FrameLabel -> {{None,None},{None,None}},FrameTicks -> {{Automatic,Automatic},{Automatic,Automatic}},GridLines -> {None,None},GridLinesStyle -> Directive[
GrayLevel[0.5,0.4]],Method -> CompressedData["1:eJzlUstOwzAQbMv7wlscuPENfAAqLYVKFKqm6t1N1u0K1xt5bYnw2/wAtqs+CbkgTkRKtPbuzuxM9mZMA7lXq9V413+eka1sLE4DpyC59EEbpHDK3pPTmTBFYgsFfOQTTWdpJiymW03Xq6ZHI/IpptzVFoxILZKWOxV8Bz4YgWFfZ27j83m3VXLug6HHeuuRY+gTY0RtrKPGYGgccBDXEYqhhOhBSkgtVw4UdD7hZKr8a2W9ojIwGe8GYbjfSh6vw/QJ9S+wwkhtQ7lCDbypu8QqaQAkmVnLGSbTowyW5pTg5kqkMANtK3HD6O9JsK2pVEmuSMLxhfQ2xelqM3rA07hKcyPOAtiYSTkL0Z8EPwD3v6sPEEkqvPhJx+m4Ucw/0F35oEVkMtTCAg+J1GseOyrlnYQpkb0XxYJiPuRhWKbFzSZCyJcULW+6mTcVbbH67Ykii/UNgX1hbEkBLsH/jKFkzS6ieTlCNhLKwX9y4gucBRzd"],PlotRange -> {{-5,5},{-0.2499995722458745,29.999997755102083}},PlotRangeClipping -> True,PlotRangePadding -> {{Scaled[
0.02],Scaled[0.02]},{Scaled[0.02],Scaled[0.02]}},Ticks -> {Automatic,Automatic}},
ExpressionUUID -> "137e5281-31ea-495a-a363-b4bdc26f926e"],StandardForm],"Output",
CellLabel -> "Out[2]= ",ExpressionUUID -> "e0d34fee-3cdf-4c41-bc4b-20b4b5e35437"]},
Open],ExpressionUUID -> "dc4f31f8-f53c-44f0-a61e-5a2013835e3a"],Cell[
BoxData[RowBox[
{"Plot","[",RowBox[{RowBox[{RowBox[{"x","^",RowBox[{"(","2",")"}]}]," ","+"," ","x"}],","," ",RowBox[
{"{",RowBox[{"x",","," ",RowBox[{"-","5"}],","," ","5"}],"}"}]}],"]"}]],"Input",ExpressionUUID -> "9a7593b9-a8a5-4fdb-9068-42e4e51d994d"],Cell[
CellGroupData[
{Cell[
BoxData[RowBox[{"Manipulate","[",RowBox[{RowBox[{"Plot","[",RowBox[{RowBox[
{RowBox[{"a"," ",RowBox[{"x","^",RowBox[{"(","2",")"}]}]}]," ","+"," ","x"}],","," ",RowBox[
{"{",RowBox[{"x",","," ",RowBox[{"-","5"}],","," ","5"}],"}"}]}],"]"}],",",RowBox[
{"{",RowBox[{"a",","," ",RowBox[{"-","10"}],","," ","10"}],"}"}]}],"]"}]],"Input",
CellLabel -> "In[4]:= ",ExpressionUUID -> "7c06d237-5896-4e92-926f-3a494226b456"],Cell[
BoxData[
TagBox[StyleBox[DynamicModuleBox[{Set[a$$,-2.5599999999999996`],Set[Typeset`show$$,
True],Set[Typeset`bookmarkList$$,{}],Set[Typeset`bookmarkMode$$,"Menu"],Typeset`animator$$,Set[
Typeset`animvar$$,1],Set[Typeset`name$$,"\"untitled\""],Set[Typeset`specs$$,{{Hold[
a$$],-10,10}}],Set[Typeset`size$$,Automatic],Set[Typeset`update$$,0],Set[Typeset`initDone$$,
False],Set[Typeset`skipInitDone$$,True]},DynamicBox[Manipulate`ManipulateBoxes[1,
StandardForm,RuleDelayed["Variables",{Set[a$$,-10]}],RuleDelayed["ControllerVariables",
{}],RuleDelayed["OtherVariables",{Typeset`show$$,Typeset`bookmarkList$$,Typeset`bookmarkMode$$,Typeset`animator$$,Typeset`animvar$$,Typeset`name$$,Typeset`specs$$,Typeset`size$$,Typeset`update$$,Typeset`initDone$$,Typeset`skipInitDone$$}],
RuleDelayed["Body",Plot[Plus[Times[a$$,Power[x,2]],x],{x,-5,5}]],RuleDelayed["Specifications",
{{a$$,-10,10}}],RuleDelayed["Options",{}],RuleDelayed["DefaultOptions",{}]],SingleEvaluation -> True],
RuleDelayed[DynamicModuleValues,{}],RuleDelayed[Deinitialization,None],RuleDelayed[
UntrackedVariables,{Typeset`size$$}],SynchronousInitialization -> True,RuleDelayed[
UnsavedVariables,{Typeset`initDone$$}],RuleDelayed[UndoTrackedVariables,{Typeset`show$$,Typeset`bookmarkMode$$}],
ExpressionUUID -> "1729e6be-1bff-4981-bfaa-476b9be0e28a"],"Manipulate",Deployed -> True,
StripOnInput -> False],Manipulate`InterpretManipulate[1]],StandardForm],"Output",
CellLabel -> "Out[4]= ",ExpressionUUID -> "1e807a4d-19a3-4e27-ac7c-a1a8fbc528c5"]},
Open],ExpressionUUID -> "12762172-da22-4a94-9145-dd43df6da4f0"],Cell[
CellGroupData[
{Cell[
BoxData[RowBox[{"Manipulate","[",RowBox[{RowBox[{"Plot","[",RowBox[{RowBox[
{RowBox[{"a"," ",RowBox[{"x","^",RowBox[{"(","2",")"}]}]}]," ","+"," ","x"}],","," ",RowBox[
{"{",RowBox[{"x",","," ",RowBox[{"-","5"}],","," ","5"}],"}"}],","," ",RowBox[{"PlotRange"," ","\[Rule]"," ",RowBox[
{"{",RowBox[{RowBox[{"{",RowBox[{RowBox[{"-","5"}],","," ","5"}],"}"}],","," ",RowBox[
{"{",RowBox[{RowBox[{"-","10"}],",","10"}],"}"}]}],"}"}]}]}],"]"}],",",RowBox[{"{",RowBox[
{"a",","," ",RowBox[{"-","10"}],","," ","10"}],"}"}]}],"]"}]],"Input",CellLabel -> "In[1]:= ",
ExpressionUUID -> "dd976bb0-64b5-477f-af51-911c6cb35ecb"],Cell[
BoxData[TagBox[StyleBox[
DynamicModuleBox[{Set[a$$,6.16`],Set[Typeset`show$$,True],Set[Typeset`bookmarkList$$,
{}],Set[Typeset`bookmarkMode$$,"Menu"],Typeset`animator$$,Set[Typeset`animvar$$,1],Set[
Typeset`name$$,"\"untitled\""],Set[Typeset`specs$$,{{Hold[a$$],-10,10}}],Set[Typeset`size$$,
Automatic],Set[Typeset`update$$,0],Set[Typeset`initDone$$,False],Set[Typeset`skipInitDone$$,
True]},DynamicBox[Manipulate`ManipulateBoxes[1,StandardForm,RuleDelayed["Variables",
{Set[a$$,-10]}],RuleDelayed["ControllerVariables",{}],RuleDelayed["OtherVariables",
{Typeset`show$$,Typeset`bookmarkList$$,Typeset`bookmarkMode$$,Typeset`animator$$,Typeset`animvar$$,Typeset`name$$,Typeset`specs$$,Typeset`size$$,Typeset`update$$,Typeset`initDone$$,Typeset`skipInitDone$$}],
RuleDelayed["Body",Plot[Plus[Times[a$$,Power[x,2]],x],{x,-5,5},PlotRange -> {{-5,5},{-10,10}}]],
RuleDelayed["Specifications",{{a$$,-10,10}}],RuleDelayed["Options",{}],RuleDelayed[
"DefaultOptions",{}]],SingleEvaluation -> True],RuleDelayed[DynamicModuleValues,{}],
RuleDelayed[Deinitialization,None],RuleDelayed[UntrackedVariables,{Typeset`size$$}],
SynchronousInitialization -> True,RuleDelayed[UnsavedVariables,{Typeset`initDone$$}],
RuleDelayed[UndoTrackedVariables,{Typeset`show$$,Typeset`bookmarkMode$$}],ExpressionUUID -> "c4fc42fc-0cb3-4793-a16e-1ebc5c89c633"],
"Manipulate",Deployed -> True,StripOnInput -> False],Manipulate`InterpretManipulate[
1]],StandardForm],"Output",CellLabel -> "Out[1]= ",ExpressionUUID -> "a1ef185c-33d3-4f31-a4da-25ea23191e00"]},
Open],ExpressionUUID -> "99b3176d-3986-4821-b7dc-18282a8a411d"],Cell[
CellGroupData[
{Cell[
BoxData[RowBox[{"Manipulate","[","\n",RowBox[{RowBox[{"Plot","[",RowBox[{RowBox[
{RowBox[{"a"," ",RowBox[{"x","^",RowBox[{"(","2",")"}]}]}]," ","+"," ",RowBox[{"b"," ","x"}]," ","+"," ","c"}],","," ",RowBox[
{"{",RowBox[{"x",","," ",RowBox[{"-","5"}],","," ","5"}],"}"}],","," ","\n",RowBox[
{"PlotRange"," ","\[Rule]"," ",RowBox[{"{",RowBox[{RowBox[{"{",RowBox[{RowBox[{"-","6"}],","," ","6"}],"}"}],","," ",RowBox[
{"{",RowBox[{RowBox[{"-","10"}],",","10"}],"}"}]}],"}"}]}]}],"]"}],",","\n",RowBox[
{"{",RowBox[{"a",","," ",RowBox[{"-","10"}],","," ","10"}],"}"}],","," ",RowBox[{"{",RowBox[
{"b",","," ",RowBox[{"-","10"}],","," ","10"}],"}"}],","," ",RowBox[{"{",RowBox[{"c",","," ",RowBox[
{"-","10"}],","," ","10"}],"}"}]}],"]"}]],"Input",CellLabel -> "In[7]:= ",ExpressionUUID -> "fba19e15-fa64-4ee3-8780-41422207ce57"],Cell[
BoxData[
TagBox[StyleBox[DynamicModuleBox[{Set[a$$,-10],Set[b$$,-10`],Set[c$$,-10`],Set[Typeset`show$$,
True],Set[Typeset`bookmarkList$$,{}],Set[Typeset`bookmarkMode$$,"Menu"],Typeset`animator$$,Set[
Typeset`animvar$$,1],Set[Typeset`name$$,"\"untitled\""],Set[Typeset`specs$$,{{Hold[
a$$],-10,10},{Hold[b$$],-10,10},{Hold[c$$],-10,10}}],Set[Typeset`size$$,Automatic],Set[
Typeset`update$$,0],Set[Typeset`initDone$$,False],Set[Typeset`skipInitDone$$,True]},
DynamicBox[Manipulate`ManipulateBoxes[1,StandardForm,RuleDelayed["Variables",{Set[
a$$,-10],Set[b$$,-10],Set[c$$,-10]}],RuleDelayed["ControllerVariables",{}],RuleDelayed[
"OtherVariables",{Typeset`show$$,Typeset`bookmarkList$$,Typeset`bookmarkMode$$,Typeset`animator$$,Typeset`animvar$$,Typeset`name$$,Typeset`specs$$,Typeset`size$$,Typeset`update$$,Typeset`initDone$$,Typeset`skipInitDone$$}],
RuleDelayed["Body",Plot[Plus[Times[a$$,Power[x,2]],Times[b$$,x],c$$],{x,-5,5},PlotRange -> {{-6,6},{-10,10}}]],
RuleDelayed["Specifications",{{a$$,-10,10},{b$$,-10,10},{c$$,-10,10}}],RuleDelayed[
"Options",{}],RuleDelayed["DefaultOptions",{}]],SingleEvaluation -> True],RuleDelayed[
DynamicModuleValues,{}],RuleDelayed[Deinitialization,None],RuleDelayed[UntrackedVariables,
{Typeset`size$$}],SynchronousInitialization -> True,RuleDelayed[UnsavedVariables,
{Typeset`initDone$$}],RuleDelayed[UndoTrackedVariables,{Typeset`show$$,Typeset`bookmarkMode$$}],
ExpressionUUID -> "5fd4c1df-5b1d-4c57-9df5-819c4c5d4c0e"],"Manipulate",Deployed -> True,
StripOnInput -> False],Manipulate`InterpretManipulate[1]],StandardForm],"Output",
CellLabel -> "Out[7]= ",ExpressionUUID -> "368a8165-b911-4972-bc01-65ef7ac54e4e"]},
Open],ExpressionUUID -> "59631223-b3e5-4702-ab0c-251643868d43"]},Open],ExpressionUUID -> "4e617fcd-b0e4-4be7-91aa-6057472862e2"]},
StyleDefinitions -> "Default.nb",FrontEndVersion -> "12.2 for Wolfram Cloud 1.57.0.2 (December 8, 2020)"]