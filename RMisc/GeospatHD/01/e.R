# mapview

# name of the shapefile of North Carolina of the sf package
nameshp <- system.file("shape/nc.shp", package="sf")

# read shapefile with st_read()
library(sf)
map <- st_read(nameshp, quiet=TRUE)
class(map)

library(mapview)
mapview(map, zcol="SID74")

library(RColorBrewer)
pal <- colorRampPalette(brewer.pal(9, "YlOrRd"))
mapview(map, 
        zcol = "SID74",
        map.types = "CartoDB.DarkMatter",
        col.regions = pal)