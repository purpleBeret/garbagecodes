# tmap

library(sf)
library(tmap)

# name of the shapefile of North Carolina of the sf package
nameshp <- system.file("shape/nc.shp", package="sf")

# read shapefile with st_read()
map <- st_read(nameshp, quiet=TRUE)
class(map)

tmap_mode("view")
tm_shape(map) + tm_polygons("SID74")
