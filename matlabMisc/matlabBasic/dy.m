function yderivative = dy(t,y)
%     This file dy.m
    yderivative = -2*y*t;
end