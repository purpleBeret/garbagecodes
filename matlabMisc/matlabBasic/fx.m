function x_sol = fx(x)
% this is file fx.m
    x_sol = (x - 3).*x - 7;
end