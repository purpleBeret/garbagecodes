% File six_1.m
% Reads in a grade and writes out
%  if the student passes or fails.
% 
% Reads in the grade
calif = input('Give me the grade:\n');
if calif >= 7.5
    fprintf('Congratulations, you passed.\n')
end
if calif < 7.5
    fprintf('I am sorry. You failed.\n')
end   