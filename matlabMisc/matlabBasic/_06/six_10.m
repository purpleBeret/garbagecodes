% File six_10.m
% Evaluation of the factorial of an integer
% It includes messages in the case the number given
% is non-negative integer

n = input('Enter an integer number n: \n');
error = 0;
if floor(n) ~= n % check if n is an integer number
%     floor(n) Evaluates the integer part of n
    error = 1; % n is not an integer number
end
if n < 0 % checks if n is an integer number
    error = 2;
end
if error == 1
    fprintf('The number entered is not an integer one.\n');
elseif error == 2
    fprintf('The number entered is a negative.\n');
elseif error ~= 1 & error ~= 2
    x = factorial1(n);
    fprintf('The factorial of %g is %g.\n', n, x)
end