function x = change1()
    global a
    fprintf('Value of "a" after entering change1 a = %g \n', a)
    a = 7;
    fprintf('Value of "a" modified in change1 a = %g.\n', a)
end