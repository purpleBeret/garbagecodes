function y = Test(x)
% Test function to watch variables
fprintf('\nJust entered Test.\n')
a = 1; b = 10; c = 100;
y = 3;
fprintf('I''m inside the function Test. \n')
fprintf('Variables are: \n a = %g, b = %g, c = %g', a, b, c)
fprintf('\nExiting the function Test.\n')