% File six_3.m
% Reads in a grade and writes out
% if the student passes or fails
% Asks for the grade

clear; close all; clc
calif = input('Enter the grade:\n');

if calif >= 7.5
    fprintf('Congratulations. You passed.\n');
elseif calif < 7.5
    fprintf('I''m sorry. You failed.\n');
else
    fprintf('Error-You must enter a numeric grade.\n');
end