% File six_9.m
% Factorial evaluation using a function
function x = factorial1(n)
    factorial1 = 1;
    if n == 0
        factorial1 = 1;
    else
        for i = 1 : n
            factorial1 = factorial1 * i;
        end
    end
    x = factorial1;
end