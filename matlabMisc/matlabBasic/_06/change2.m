function x = change2()
    global a
    fprintf('Value of "a" after entering change2 a = %g \n', a)
    a = 12;
    fprintf('Value of "a" modified in change2 a = %g.\n', a)
end