% File six_6.m
% Factorial of a non negative integer
% 
n = input('Enter a non-negative integer: ');
n_factorial = 1;
for i = 1:n
    n_factorial = n_factorial*i;
end
fprintf('The factorial of %g is %g. \n', n, n_factorial);