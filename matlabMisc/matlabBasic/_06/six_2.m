% File six_2.m
% Reads in a grade and writtes out
% if the student passes or fails

clear; close; clc

% Reads in the grade
calif = input('Enter the grade:\n');
if calif >= 7.5
    fprintf('Congratulations. You passed. \n')
else
    fprintf('I''m sorry. You failed \n')
end