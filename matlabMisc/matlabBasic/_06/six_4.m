% File six_4.m
% indicating if the student passes or fails
% 
% Enter the grade
calif = input('Enter the grade:\n');
switch calif
    case {7.5, 8, 8.5, 9, 9.5, 10}
        fprintf('Congratulations. You passed \n')
    case {0, 0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5}
        fprintf('I''m sorry. You failed.\n')
    otherwise
        fprintf('It was not enough. You failed.\n')
end        
