% File six_8.m
% 
% It evaluates the volume of spheres with radii 1 to 5
%
% r is the radius of sphere
r = 0;
while r < 5
    vol = (4/3) * pi * r^3;
    fprintf('The radius is %g and the volume is %g.\n', r, vol);
    r = r + 1;
end