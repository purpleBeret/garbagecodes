% File six_7.m
% This m-file evaluates the sum of
% the elements of a matrix n x m.
n = input('Enter the number of rows: \n');
m = input('Enter the number of columns: \n');
% Reads in the elements
% Initialize the sum
sum = 0;
% Reads in the elements of matrix and adds them.
for i = 1 : n 
    % Reads in the elements of row i and adds them.
    for j = 1 : m
        fprintf('Enter the matrix element %g, %g', i,j)
        a(i,j) = input('\n');
        sum = sum + a(i,j);
    end
end
fprintf('The total sum is %g\n', sum)