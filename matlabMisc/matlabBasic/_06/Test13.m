function [a, b, c] = Test13(a,b,c)
fprintf('Variables in Test13 are: \n')
fprintf('a = %g b = %g c = %g', a,b,c)
a = a * 2;
b = b * 10;
c = c * 100;
y = [a, b, c];
fprintf('\nI''m inside Test13.\n');
fprintf('Variables have been changed to \n');
fprintf('a = %g, b = %g, c = %g', a, b, c);
fprintf('\nExiting the function.');
