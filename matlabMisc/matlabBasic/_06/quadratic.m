% Function quadratic.m
function [x1, x2] = quadratic(a,b,c)
% This function evaluates the roots x1, x2
% of the quadratic equation ax^2 + bx + c = 0
% The file is quadratic.m
discriminant = b^2 - 4*a*c;
x1 = (-b + sqrt(discriminant)) / 2*a;
x2 = (-b - sqrt(discriminant)) / 2*a;
