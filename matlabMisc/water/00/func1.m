function [Area, Perimeter] = func1(Radius)
    Area = (Radius ^ 2) * pi;
    Perimeter = 2 * Radius * pi;
    disp('Area = '); disp(Area);
    disp('Perimeter = '); disp(Perimeter);
end
