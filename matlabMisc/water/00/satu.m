% This program calculates the area and perimeter of a rectangle

a = input('Length of rectangle: ');
b = input('Width of rectangle: ');

Area = a * b;
perimeter = 2 * (a + b);

disp('Area = '); disp(Area);
disp('Perimeter = '); disp(perimeter);