% Adaptive Filter Test
% copy from Trauth (2020)

clear; close all; clc

rng(0)
x = 0:0.1:100;
x = x';
y = sin(x);
yn1 = y + 0.5*randn(size(y));
yn2 = y + 0.5*randn(size(y));

k = kron(yn1,yn1');
u = 1 / max(eig(k));
l = 5;
iter = 20;

[z, e, mer, w] = canc(yn1, yn2,u,l,20)
