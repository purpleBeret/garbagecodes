function canctool(varargin)
% CANCTOOL MATLAB code for canctool.fig
%CANCTOOL  Correlated Data Adaptive Noise Cancelling Algorithm.
%   CANCTOOL(Y1,Y2) extracts the noise-free signal from two vectors Y1 and
%   Y2 containing the correlated signals and uncorrelated noise using an
%   adaptive noise cancelling algorithm.
%
%   The function uses the following internal variables:
%
%   s     Primary signal
%   x     Reference signal
%   w     Filter weights
%   iter  Number of filter loops
% 
%   The function exports the following variables to the workspace:
%
%   yy    Filtered primary signal
%   zz    Filtered reference signal using the same filter weights
%   ee    Noise extracted from the primary signal
%   mer   Mean-squared error for learning curve
%
%   Example: 
%
%   x = 0 : 0.1 : 100;
%   y = sin(x);
%   yn1 = y + 0.5*randn(size(y));
%   yn2 = y + 0.5*randn(size(y));
%   canctool(yn1,yn2)
%
%   References:
%   Hattingh, M. (1988): A new data adaptive filtering program to remove
%   noise from geophysical time- or space-series data. Computers and
%   Geosciences, 14, 467-480.
%   Trauth, M.H. (1998): Noise removal from duplicate paleoceanographic
%   time-series: The use of adaptive filtering techniques. Mathematical
%   Geology,30(5), 557-574.

% Last Modified by GUIDE v2.5 05-Dec-2013 15:33:15

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @canctool_OpeningFcn, ...
                   'gui_OutputFcn',  @canctool_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before canctool is made visible.
function canctool_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to canctool (see VARARGIN)

% Choose default command line output for canctool
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes canctool wait for user response (see UIRESUME)
% uiwait(handles.figure1);

global x s u l iter

% Default values
u    = 0.00001;
l    = 11;
iter = 100;

% Collect s and x from varargin
s = varargin{1};
x = varargin{2};


% --- Outputs from this function are returned to the command line.
function varargout = canctool_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double
global l
l = str2double(get(hObject,'String'));

% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double
global u
u = str2double(get(hObject,'String'));

% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit3 as text
%        str2double(get(hObject,'String')) returns contents of edit3 as a double
global iter
iter = str2double(get(hObject,'String'));

% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global x s u l iter

[n1,n2]=size(s);n=n2;index=0; % Formatting
if n1>n2
    s=s';x=x';n=n1;index=1;
end

plot(handles.Original_Signal_Axis,1:length(s),s,1:length(x),x)
title(handles.Original_Signal_Axis,['Original Signals'])
XLim = get(handles.Original_Signal_Axis,'XLim');
YLim = get(handles.Original_Signal_Axis,'YLim');

w(1:l)=zeros(1,l);e(1:n)=zeros(1,n); % Initialization
xx(1:l)=zeros(1,l);ss(1:l)=zeros(1,l);
z(1:n)=zeros(1,n);y(1:n)=zeros(1,n);
ors=s;ms(1:n)=mean(s).*ones(size(1:n));
s=s-ms;x=x-ms;ors=ors-ms;

clear mer

for it=1:iter % Iterations
    for I=(l+1):(n+1) % Filter loop
		for k=1:l
            xx(k)=x(I-k);ss(k)=s(I-k);
        end
        for J=1:l
            y(I-1)=y(I-1)+w(J).*xx(J);
            z(I-1)=z(I-1)+w(J).*ss(J);
        end
            e(I-1)=ors(I-1-(fix(l/2)))-y(I-1);
        for J=1:l
            w(J)=w(J)+2.*u.*e(I-1).*xx(J);
        end
    end % End filter loop
	for I=1:n % Phase correction
		if I<=fix(l/2)
			yy(I)=0;zz(I)=0;ee(I)=0;
		elseif I>n-fix(l/2)
			yy(I)=0;zz(I)=0;ee(I)=0;
		else
			yy(I)=y(I+fix(l/2));
            zz(I)=z(I+fix(l/2));
			ee(I)=abs(e(I+fix(l/2)));
		end
			yy(I)=yy(I)+ms(I);
            zz(I)=zz(I)+ms(I);
    end % End phase correction
	y(1:n)=zeros(size(1:n));
	z(1:n)=zeros(size(1:n));
    
    if it < iter
    plot(handles.Filtered_Signal_Axis,1:length(yy),yy)
    title(handles.Filtered_Signal_Axis,['Filter output after iteration ',num2str(it)])
    set(handles.Filtered_Signal_Axis,'XLim',XLim)
    set(handles.Filtered_Signal_Axis,'YLim',YLim)
    else
    plot(handles.Filtered_Signal_Axis,1:length(yy),yy,1:length(ee),ee)
    title(handles.Filtered_Signal_Axis,['Filter output after iteration ',num2str(it)])
    set(handles.Filtered_Signal_Axis,'XLim',XLim)
    set(handles.Filtered_Signal_Axis,'YLim',YLim)
    end
    
    mer(it)=mean(ee((fix(l/2)):(n-fix(l/2))).^2);
    pause(0.2)
end % End iterations
if index==1 % Reformatting
    zz=zz';yy=yy';ee=ee'; mer=mer';
end

plot(handles.Mean_Squared_Error_Axis,mer)
title(handles.Mean_Squared_Error_Axis,['Learning Curve'])

% Export results to workspace
assignin('base','yy' ,yy)
assignin('base','zz' ,zz)
assignin('base','ee' ,ee)
assignin('base','mer',mer)
















