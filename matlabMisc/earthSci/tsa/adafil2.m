% Adaptive Filter GUI
clear; close all; clc

x = 0:0.1:100; x = x';
y = sin(x);

rng(0)
yn1 = y + 0.5 * randn(size(y));
yn2 = y + 0.5 * randn(size(y));

canctool(yn1, yn2)