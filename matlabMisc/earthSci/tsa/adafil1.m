% Adaptive Filter Test

clear; close all; clc

x = 0:0.1:100; x = x';
y = sin(x);

rng(0)
yn1 = y + 0.5*randn(size(y));
yn2 = y + 0.5*randn(size(y));

yn1(501:1001) = y(501:1001);
yn2(501:1001) = y(501:1001);

plot(x, yn1, x, yn2);

k = kron(yn1, yn1');
u = 1/max(eig(k));

[z, e, mer, w] = canc(yn1, yn2, 0.0016, 5, 20);
plot(mer)
plot(x, y, 'b', x, z, 'r')
surf(w(3:999,:)), shading interp