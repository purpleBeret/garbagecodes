clear; close all; clc

syms y(t)
Dy = diff(y, t);
D2y = diff(y, t, 2);

fprintf('To solve: A*ddy + B*dy + C*y = D at ICs: y(0) = a, dy(0) =b \n')
fprintf('Enter the values of [A, B, C, D] and [a, b] \n')

A = input('Enter A = ');
B = input('Enter B = ');
C = input('Enter C = ');
D = input('Enter D = ');
a = input('Enter a = ');
b = input('Enter b = ');

% Given ODE equation

eqn = A*D2y + B*Dy + C*y == D;
ICs = [y(0) == a; Dy(0) == b];
y_t = dsolve(eqn, ICs);

% Display the computed analytical solution in the command window
fplot(y_t, [0, 5], 'b-'), grid
xlabel('\it t')
ylabel('\it Solution, y(t)')
title('\it Solution of : $$\frac{A dy}{dt^2}+\frac{B dy}{dt}+ C y = D$$', 'Interpreter', 'latex')