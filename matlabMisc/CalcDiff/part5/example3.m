function yex3 = example3(x,y)
    yex3 = [y(2); -y(1) * y(2) - y(1)];
end