function yex2 = example2(x,y)
    yex2 = [y(2); 20*cos(x) - 4*y(2) - 3*y(1)];
end