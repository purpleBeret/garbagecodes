function yex3 = example3(x,y)
    yex3 = [y(2); y(3); y(4);
        (sin(x) - x/2 - 18*y(1) - 442*y(3))/49];
end