function yex2 = example2(x,y)
    yex2 = [y(2); y(3); y(4); (sin(x) - x/2 -9*y(1) - 442*y(3))/49];
end