function yex1 = example1(x,y)
    yex1 = [y(2); y(3); [y(1) - 10*y(2) + y(3)]];
end