// This script calculates the time series for the logistic equation
// It then prints the time series to the browser and makes
// a time series plot
//
// David P. Feldman
// Feb 2014
//
// This work is licensed under a Creative Commons 
// Attribution-NonCommercial-ShareAlike 3.0 Unported License.
// http://creativecommons.org/licenses/by-nc-sa/3.0/
//
// You are free:
//
//    to Share — to copy, distribute and transmit the work
//    to Remix — to adapt the work
//
// Under the following conditions:
//
//    Attribution — You must attribute the work in the manner specified 
//    by the author or licensor (but not in any way that suggests that 
//    they endorse you or your use of the work).
//
//    Noncommercial — You may not use this work for commercial purposes.
//
//    Share Alike — If you alter, transform, or build upon this work, you 
//    may distribute the resulting work only under the same or similar 
//    license to this one. 


function fX(x,y,a)
{
    // return the value of x_n+1
    //return (+r)*(+x)*(1-x); logistic
    return 1 - (+a)*(+x)*(+x) + (+y); 
}
function fY(x,y,b)
{
    // return the value of x_n+1
    //return (+r)*(+x)*(1-x); logistic
    return (+b)*(+x)
}

function roundNumber(num, dec) {
    // function that rounds num to dec decimal places
    var result = Math.round(num*Math.pow(10,dec))/Math.pow(10,dec);
    //result = num.toFixed(dec);
    // i think both methods work.  not sure which one i should use
    return result;
}

function timeseries(n,x0,y0,a,b,SKIP) 
{
    //NOTE TO SELF:  I NEED TO IMPLEMENT THE BOUNDS CHECKING ERROR MESSAGES STUFF!!
    r = 3.0;  // delete this once i get rid of all references to r
    var error_msg="<br>";
    //document.getElementById("error").innerHTML=error_msg;
    //perform some sanity checks
    if(n>10000){
	error_msg = error_msg +  "<b>Error: n must be less than 10,000.</b><br><br>";
	n=0;
	//document.getElementById("error").innerHTML=error_msg;
    }
   if(r<0 || r>4.0){
	error_msg = error_msg + "<b>Error: r must be between 0 and 4.0.</b><br><br>";
	n=0;
    }
   if(x0<0 || x0>1){
	error_msg = error_msg + "<b>Error: x<sub>0</sub> must be between 0 and 1.0.</b><br><br>";
	n=0;
    }
    document.getElementById("error").innerHTML=error_msg;
    

    var TX = [ [0,x0] ] // holds the timeseries
    var TY = [ [0,y0] ] // holds the timeseries
    var XY = [ [] ] // holds the data for the phase plane
    if(SKIP==0){ // if SKIP is 0, add initial conditions to the X-Y plot
	XY.push([x0,y0]);
    }
    var x = (+x0);      // initial value of x is 0
    var y = (+y0);      // initial value of x is 0
    var xNew = 0;
    // Prepare the output for reporting
    var msg = '<table border="0.5"><tr><td width="150">Time <i>t</i></td><td width="200">x<sub>t</sub></td><td witdh="200">y<sub>t</sub></tr>';
    msg += '<tr><td>0</td><td>'+x0+'</td><td>'+y0+'</tr>';        
    // loop for required number of iterations
    for (var t = 1; t <= n; t++)
    {
        xNew = fX(x,y,a);
	y = fY(x,y,b);
	x = xNew;
	TX.push([t,x]);
	TY.push([t,y]);
	if(t>SKIP){
	    XY.push([x,y]);
	}
        // record the t, x_t, and y_t values
        msg += '<tr><td>'+t+'</td><td>'+roundNumber(x,5)+'</td><td>'+roundNumber(y,5)+'</tr>';
//        msg += '<tr><td>'+t+'</td><td>'+x+'</td></tr>';
    }
    msg += '</table>';
    // print out the time series values
    document.getElementById("msg").innerHTML=msg;
    // Make the X timeseries plot
    // I first need to set delta T, the spacing on the x axis
    var deltaT = 2
    if(n<=40)
	deltaT=2;
    if(n<20)
	deltaT=1;
    if(n>40)
	deltaT = Math.ceil(n/10)    
    
    $(document).ready(function(){
	// first clear out the old plot
	$('#placeholderX').empty();
	var plot1 = $.jqplot ('placeholderX',[TX], {
	    series:[{
		showMarker:true,
		color:"purple"
	    }],
	    axesDefaults: {
		labelRenderer: $.jqplot.CanvasAxisLabelRenderer
	    },
	    axes:{
		xaxis:{
		    label:"Time t",
		    min:0,
		    max:n,
		    pad: 20,
		    tickInterval:deltaT,
		    labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
		    labelOptions:{
			    fontSize: '16pt',
			    textColor: '#333333'
			}
		},
		yaxis:{
		    label:"x_t",
		    //min:-1.5,
		    //max:1.5,
		    labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
		    labelOptions:{
			    fontSize: '16pt',
			    textColor: '#333333'
			}
		},
	    }
	}
			     );
    });

    // Make the Y timeseries plot
    // I first need to set delta T, the spacing on the x axis
     
    $(document).ready(function(){
	// first clear out the old plot
	$('#placeholderY').empty();
	var plot1 = $.jqplot ('placeholderY',[TY], {
	    series:[{
		showMarker:true,
		color:"purple"
	    }],
	    axesDefaults: {
		labelRenderer: $.jqplot.CanvasAxisLabelRenderer
	    },
	    axes:{
		xaxis:{
		    label:"Time t",
		    min:0,
		    max:n,
		    pad: 20,
		    tickInterval:deltaT,
		    labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
		    labelOptions:{
			    fontSize: '16pt',
			    textColor: '#333333'
			}
		},
		yaxis:{
		    label:"y_t",
		    //min:-0.45,
		    //max:0.45,
		    labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
		    labelOptions:{
			    fontSize: '16pt',
			    textColor: '#333333'
			}
		},
	    }
	}
			     );
    });

 
   // MAKE THE Y VS X PLOT!!
    $(document).ready(function(){
	// first clear out the old plot
	$('#placeholderXY').empty();
	var plot1 = $.jqplot ('placeholderXY',[XY], {
	    series:[{
		markerOptions:{size:6},
		showMarker:true,
		showLine:false,
		color:"blue"
	    }],
	    axesDefaults: {
		labelRenderer: $.jqplot.CanvasAxisLabelRenderer
	    },
	    axes:{
		xaxis:{
		    label:"x",
		    //min:-1.5,
		    //max:1.5,
		    //pad: 20,
		    //tickInterval:0.2,
		    labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
		    labelOptions:{
			    fontSize: '16pt',
			    textColor: '#333333'
			}
		},
		yaxis:{
		    label:"y",
		    //min:-0.44,
		    //max:0.44,
		    labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
		    labelOptions:{
			    fontSize: '16pt',
			    textColor: '#333333'
			}
		},
	    }
	}
			     );
    });

    
    //plot1.replot({resetAxes:true});
}
	 
