% MATLAB's syntax for what a function call should looks like:
% function[stuff_to_return] = function_name(arguments)
function [iterates] = logistic(x0, r, n)
    iterates = []; % set up an [empty] vector in which to store the iterates
    currentX = x0; % this is the internal variable that stores the current iterate
    for index = [0:n] % for index = 0, 1, ..., n
        % stick the current iterate onto the tail end of the vector of
        % iterates:
        iterates = [iterates, currentX];
        % compute the next iterate and stick it in the current-iterate
        % variable:
        currentX = r * currentX * (1 - currentX);
        % end loop
    end
end