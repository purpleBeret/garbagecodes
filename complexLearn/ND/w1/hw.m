clear; close all; clc

% 1
traj1 = logistic(0.2, 2, 200);
traj2 = logistic(0.200001, 2, 200);
diff = abs(traj1 - traj2);
n_values = [0:200];
figure(1);
plot(n_values, diff), grid

traj1 = logistic(0.2, 3.4, 200);
traj2 = logistic(0.200001, 3.4, 200);
diff = abs(traj1 - traj2);
n_values = [0:200];
figure(2);
plot(n_values, diff), grid

traj1 = logistic(0.2, 3.72, 200);
traj2 = logistic(0.200001, 3.72, 200);
diff = abs(traj1 - traj2);
n_values = [0:200];
figure(3);
plot(n_values, diff), grid

% 2
traj1 = logistic(0.2, 2, 500);
traj2 = logistic(0.200001, 2, 500);
abs(traj1(500) - traj2(500))

traj1 = logistic(0.2, 3.4, 500);
traj2 = logistic(0.200001, 3.4, 500);
abs(traj1(500) - traj2(500))


traj1 = logistic(0.2, 3.72, 5000);
traj2 = logistic(0.200001, 3.72, 5000);
diff = abs(traj1 - traj2);
mean(diff)

traj1 = logistic(0.2, 3.72, 500000);
traj2 = logistic(0.200001, 3.72, 500000);
diff = abs(traj1 - traj2);
mean(diff)
